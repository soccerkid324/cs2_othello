1) I worked by myself so did everything by myself.

2) So the basic design of my AI is a priority system that
does the following:
-plays in the corners if possible
-plays on the edges, but not in the three tiles adjacent
to the corners
-plays in the left side of the board when possible.
-if all other moves are prohibited and the tiles adjacent
to the corners are available, plays there.

The basic idea is that the corners which can't be taken 
have the highest priority. The tiles adjacent to the
corners allow the opposing player to move in the corner
and is thus undesireable. The edges are more desireable as
they permit you to take entire rows of pieces in single
moves.  Past that, the simple playing the left-most move
1) speeds the algorithm up so I didn't have to write more
and 2) moves the AI closer to the edge where it will
benefit.

I tried to implement a smarter heuristic that takes into
account that a move in the midder 6x6 square is better 
than a move in the 2nd outer 'circle' meaning those tiles
outside the inner 6x6 square and not edges or corners.
This is because moving in the 2nd outer 'circle' allows 
the opponent to play on the edge and is thus less
desireable.  However, it didn't work properly and I got 
busy so I just reverted to not having this addition.

Things that will allow my program to win: if it gets the
left corners/edge, it will slowly move to the right side
of the board taking many of the opponents pieces as it 
goes while not allowing the opponent to retake pieces.

Why it will lose: because it's not likely to get the left
edge as I programmed it and it doesn't really do anything
else.