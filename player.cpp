#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    b = new Board();
    mySide = side;
    other = (mySide == BLACK) ? WHITE : BLACK;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
bool Player::move(Move *m)
{
	if(b->checkMove(m, mySide))
	{
		b->doMove(m, mySide);
		return true;
	}
	return false;
}
Move *Player::doMove(Move *opponentsMove, int msLeft)
{
//	if(opponentsMove != NULL)
//	{
//		Side other = (mySide == BLACK) ? WHITE : BLACK;
		b->doMove(opponentsMove, other);
//	printf("Look I'm here!");
//	}
	if(!(b->hasMoves(mySide)))
	    return NULL;
//	Board *c = b->copy();
    Move *m = new Move(0,0);
    if(move(m))
        return m;
    m = new Move(0,7);
    if(move(m))
        return m;
    m = new Move(7,0);
    if(move(m))
        return m;
    m = new Move(7,7);
    if(move(m))
        return m;       
	for(int x = 0; x < 8; x++)
	{
		for(int y = 0; y < 8; y++)
		{
			if(!((x < 2 && y < 2) || (x < 2 && y > 5) || (x > 5 && y < 2)
			   || (x > 5 && y > 5)))
			{
				m = new Move(x,y);
				if(move(m))
				    return m;
			}
		}
	}
	for(int x = 0; x < 8; x++)
	{
		for(int y = 0; y < 8; y++)
		{
			if(((x < 2 && y < 2) || (x < 2 && y > 5) || (x > 5 && y < 2)
			   || (x > 5 && y > 5)))
			{
				m = new Move(x,y);
				if(move(m))
				    return m;
			}
		}
	}

    return NULL;
}
